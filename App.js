import React, { Component } from 'react';
import Navigator from "./Navigator";
import {StatusBar, View} from "react-native";
import SplashScreen from "react-native-splash-screen";

class App extends Component {

    componentDidMount() {
        SplashScreen.hide();
    }

    render() {
        return(
            <View style={{flex:1}}>
                <StatusBar
                    backgroundColor="#6e223b"
                    barStyle="light-content"
                />
                <Navigator/>
            </View>
        )
    }
}

export default App