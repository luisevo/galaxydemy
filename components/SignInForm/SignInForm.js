import React from 'react'
import { KeyboardAvoidingView } from 'react-native'
import { reduxForm, Field } from 'redux-form'
import InputForm from "../InputForm/InputForm";
import ButtonForm from "../ButtonForm/ButtonForm";

const signInForm = (props) => {
    return (
        <KeyboardAvoidingView>
            <Field
                name="email"
                component={InputForm}
                placeholder="Email"
                keyboardType="email-address"
            />

            <Field
                name="password"
                component={InputForm}
                placeholder="Password"
                secureTextEntry={true}
            />
            <ButtonForm
                title="Iniciar Sesión"
                onPress={props.handleSubmit((values)=>{
                    props.onAuth(values);
                })}
            />
        </KeyboardAvoidingView>
    );
};

const validate = (values) => {
    const errors = {};

    if (!values.email) {
        errors.email = 'required';
    }

    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'invalid email';
    }

    if (!values.password) {
        errors.password = 'required';
    }

    return errors;
};

export default reduxForm({
    form: 'SignInForm',
    validate
})(signInForm);