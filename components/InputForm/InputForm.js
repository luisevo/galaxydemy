import React from 'react'
import {StyleSheet, TextInput, View, Text} from "react-native";

const inputForm = (props) => {
    console.log(props)
    return (
        <View>
            <TextInput
                value={props.input.value}
                onChangeText={(value) => props.input.onChange(value) }
                placeholder={props.placeholder}
                placeholderTextColor="white"
                style={styles.input}
                autoCapitalize="none"
                keyboardType={props.keyboardType ? props.keyboardType: 'default'}
                secureTextEntry={!!(props.secureTextEntry)}
                onBlur={props.input.onBlur}
            />
            {
                props.meta.touched && props.meta.error ?
                    <Text style={styles.error}>{props.meta.error}</Text> : null

            }
        </View>

    );
}


const styles = StyleSheet.create({
    input: {
        width: 300,
        fontSize: 20,
        margin: 10,
        borderBottomWidth: 1,
        borderBottomColor: 'white',
        color: 'white'
    },
    error: {
        marginHorizontal: 10,
        color: 'white'
    }
});


export default inputForm;