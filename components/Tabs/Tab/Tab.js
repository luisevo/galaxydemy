import React from 'react';
import { View, Text, StyleSheet} from 'react-native';
import TouchableWrapper from '../../TouchableWrapper/TouchableWrapper'
type Props = {
    title: string,
}
const tab = (props: Props) => (
    <TouchableWrapper>
        <View style={styles.tab}>
            <Text style={styles.tabTitle} >{props.title}</Text>
        </View>
    </TouchableWrapper>
);

const styles = StyleSheet.create({
    tab: {
        flex: 1,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    tabTitle: {
        fontSize: 18,
        color: 'white'
    }
});

export default tab;