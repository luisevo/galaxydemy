import {AsyncStorage} from "react-native"
export const LOADING_SUBJECTS = '[Subject] Loading';
export const LOADED_SUBJECTS_SUCCESS = '[Subject] Loaded Success';
export const LOADED_SUBJECTS_ERROR = '[Subject] Loaded error';

export const loadingSubjects = () => ({
   type: LOADING_SUBJECTS
});

export const loadedSubjectsSuccess = (subjects) => ({
    type: LOADED_SUBJECTS_SUCCESS,
    payload: {subjects}
});

export const loadedSubjectsError = (error) => ({
    type: LOADED_SUBJECTS_ERROR,
    payload: {error}
});

export const loadSubjects = () => {
    return async (dispatch) => {
        dispatch(loadingSubjects());

        const token = await AsyncStorage.getItem('token');

        fetch(`https://galaxydemy.firebaseio.com/subjects.json?auth=${token}`)
            .then(async res => {
                if (res.status === 200) return res.json();
                else throw await res.json()
            })
            .then(res => {
                dispatch(loadedSubjectsSuccess(res));
            })
            .catch(err => {
                dispatch(loadedSubjectsError(err));
            })
    }
};
