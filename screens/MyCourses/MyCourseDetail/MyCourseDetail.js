import React, {Component} from 'react';
import { View, Text, StyleSheet, Image, Dimensions } from 'react-native';

const {width} = Dimensions.get('window');

class MyCourseDetail extends Component{
    static navigationOptions = ({navigation}) => {
      const {title} = navigation.getParam('subject');
      return {
          title: title
      }
    };
    render(){
        const subject = this.props.navigation.getParam('subject');
        return(
            <View style={styles.container}>
                <Image
                    style={{width: width, height: 200}}
                    source={{uri: subject.img}} />
                <View style={styles.content}>
                    <Text style={styles.title} >{subject.title}</Text>
                    <Text style={styles.subtitle}>{subject.instructor}</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    content: {
        padding: 10,
        flex: 1
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold'
    },
    subtitle: {
        fontSize: 20
    }
});



export default MyCourseDetail;
