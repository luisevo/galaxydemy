import React, {Component} from 'react';
import {View, AsyncStorage, ActivityIndicator, StyleSheet} from 'react-native';
import LinearGradient from "react-native-linear-gradient";

class ValidateAuth extends Component {

    componentDidMount = async () => {
      const token = await AsyncStorage.getItem('token');
      this.props.navigation.navigate(token ? 'Menu' : 'SignIn');
    };

    render() {
        return (
            <LinearGradient style={styles.container} colors={['#6e223b', '#d84d4d']}>
                <ActivityIndicator/>
            </LinearGradient>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default ValidateAuth;