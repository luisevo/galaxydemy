import React, {Component} from 'react';
import {View, Text, Button, AsyncStorage, StyleSheet} from 'react-native';

class Account extends Component {
    static navigationOptions = {
        title: 'Account'
    };
    render() {
        return (
            <View style={styles.container}>
                <Button title="Logout"
                        color="#6e223b"
                        onPress={async () => {
                            await AsyncStorage.removeItem('token');
                            this.props.navigation.navigate('SignIn')
                        }}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
   container: {
       padding: 10
   }
});

export default Account;