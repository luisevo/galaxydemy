import {combineReducers, createStore, applyMiddleware} from 'redux';
import {subjectReducer} from "./reducers/subject.reducer";
import { reducer as form } from "redux-form";
import thunk from "redux-thunk";

const rootReducer = combineReducers({
    subject: subjectReducer,
    form
});

export const generateStore = () => {
    return createStore(rootReducer, applyMiddleware(thunk));
};