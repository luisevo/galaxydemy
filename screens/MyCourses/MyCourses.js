import React, { Component } from 'react';
import { View, StyleSheet, Text} from 'react-native';
import SubjectList from "../../components/SubjectList/SubjectList";
import {connect} from "react-redux";
import {
    loadSubjects,
} from "../../store/actions/subject.actions";

class MyCourses extends Component {

    componentDidMount() {
        this.props.loadSubjects();
    }

    render() {
        return(
            <View style={styles.container}>
                {/*
                <Modal
                    visible={!!this.props.selected}
                    onRequestClose={() => {this.props.deselect()}}>
                    <View>
                        {
                            !this.props.selected ? null :
                                <Text>{ JSON.stringify(this.props.selected)}</Text>
                        }
                    </View>
                </Modal>
                <Header title="My Courses"/>
                */}
                {!this.props.error ? null :
                <Text>{ JSON.stringify(this.props.error)}</Text>}

                <SubjectList
                    onItemPress={(subject) => {
                        // this.props.select(subject)
                        this.props.navigation.navigate('MyCourseDetail', {subject})
                    }}
                    subjects={this.props.subjects}
                    refreshing={this.props.loading}
                    onRefresh={() => {
                        this.props.loadSubjects()
                    }}
                />
                {/*<Tabs/>*/}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});


const mapStateToProps = (state) => ({
    subjects: state.subject.subjects,
    loading: state.subject.loading,
    error: state.subject.error
});

const mapDispatchToProps = (dispatch) => ({
    loadSubjects: () => dispatch(loadSubjects())
});

export default connect(mapStateToProps, mapDispatchToProps)(MyCourses)