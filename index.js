/** @format */
import React from 'react';
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import {generateStore} from './store/store';
import {Provider} from 'react-redux'

const store = generateStore();

const appRedux = () => (
    <Provider store={store} >
        <App />
    </Provider>
);

AppRegistry.registerComponent(appName, () => appRedux);
