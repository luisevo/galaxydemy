import React from 'react';
import {StyleSheet, Text, View} from "react-native";
import TouchableWrapper from "../TouchableWrapper/TouchableWrapper.ios";

const buttonForm = (props) => (
    <TouchableWrapper
        onPress={props.onPress}>
        <View style={styles.button}>
            <Text style={styles.buttonText}>{props.title}</Text>
        </View>
    </TouchableWrapper>
);


const styles = StyleSheet.create({
    button: {
        backgroundColor: 'white',
        margin: 10,
        padding: 10,
        elevation: 5
    },
    buttonText: {
        fontSize: 18,
        textAlign: 'center'
    }
});


export default buttonForm;