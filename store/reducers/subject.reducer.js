import {LOADED_SUBJECTS_ERROR, LOADED_SUBJECTS_SUCCESS, LOADING_SUBJECTS} from "../actions/subject.actions";

const initialState = {
  subjects: [],
  loading: false,
  error: null
};

export const subjectReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOADING_SUBJECTS:
            return {...state, loading: true};
        case LOADED_SUBJECTS_SUCCESS:
            return {
                ...state,
                subjects: action.payload.subjects,
                loading: false,
                error: null
            };
        case LOADED_SUBJECTS_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payload.error
            };
        default:
            return state;
    }
};