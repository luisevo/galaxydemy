import React from 'react'
import {
    createAppContainer,
    createBottomTabNavigator,
    createStackNavigator,
    createSwitchNavigator
} from 'react-navigation';
import MyCourses from "./screens/MyCourses/MyCourses";
import Search from "./screens/Search/Search";
import Account from "./screens/Account/Account";
import MyCourseDetail from "./screens/MyCourses/MyCourseDetail/MyCourseDetail";
import SignIn from "./screens/SignIn/SignIn";
import Icon from 'react-native-vector-icons/FontAwesome';
import ValidateAuth from "./screens/ValidateAuth/ValidateAuth";


const defaultNavigationOptions = {
    headerStyle: {
        backgroundColor: '#6e223b'
    },
    headerTintColor: '#fff'
};

const MyCourseStack = createStackNavigator(
    {
        MyCourses: {
            screen: MyCourses,
            navigationOptions: {
                title: 'My Courses'
            }
        },
        MyCourseDetail
    },
    {defaultNavigationOptions}
);

const SearchStack = createStackNavigator(
    {Search},
    {defaultNavigationOptions}
);

const AccountStack = createStackNavigator(
    {Account},
    {defaultNavigationOptions}
);


const MenuTab = createBottomTabNavigator(
    {
        Search: {
            screen: SearchStack,
            navigationOptions: {
                tabBarIcon: ({tintColor}) => <Icon name="search" size={18} color={tintColor} />
            }
        },
        MyCourses: {
            screen: MyCourseStack,
            navigationOptions: {
                tabBarIcon: ({tintColor}) => <Icon name="play-circle-o" size={18} color={tintColor} />
            }
        },
        Account: {
            screen: AccountStack,
            navigationOptions: {
                tabBarIcon: ({tintColor}) => <Icon name="user" size={18} color={tintColor} />
            }
        },
    },
    {
        initialRouteName: 'MyCourses',
        tabBarOptions: {
            activeTintColor: '#fff',
            inactiveTintColor: '#9e9e9e',
            style: {
                backgroundColor: '#607d8b'
            },
            labelStyle: {
                fontSize: 18
            },
            showIcon: true,
            tabStyle: {
                justifyContent: 'center'
            }
        }
    }
);

const Navigator = createSwitchNavigator({
    ValidateAuth: ValidateAuth,
    SignIn: SignIn,
    Menu: MenuTab
});

export default createAppContainer(Navigator);