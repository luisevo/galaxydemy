import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Tab from "./Tab/Tab";

const tabs = (props) => (
    <View style={styles.tabs} >
        <Tab title="Search" />
        <Tab title="My Courses"/>
        <Tab title="Account"/>
    </View>
);

const styles = StyleSheet.create({
   tabs: {
       height: 56,
       flexDirection: 'row',
       justifyContent: 'space-around',
       alignItems: 'center',
       backgroundColor: '#607d8b'
   }
});

export default tabs;