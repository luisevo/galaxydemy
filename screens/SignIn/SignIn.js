
import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    ActivityIndicator,
    AsyncStorage,
    Keyboard,
    Alert
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import SignInForm from "../../components/SignInForm/SignInForm";

class SignIn extends Component {

    state = {
        loading: false,
        email: '',
        password: ''
    };

    auth = (values) => {
        Keyboard.dismiss();
        this.setState({loading: true});

        const key = 'AIzaSyDVlHVTeKVe60aETvVFuS7ujlhTPRNsedE';
        const url = `https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=${key}`;
        const params = {...values, returnSecureToken: true};
        const options = {
            method: 'POST',
            body: JSON.stringify(params)
        };
        fetch(url, options)
            .then(async res => {
                if (res.status === 200) return res.json()
                else throw await res.json()
            })
            .then(async res => {
                console.log(res);
                await AsyncStorage.setItem('token', res.idToken);
                this.setState({loading: false});
                this.props.navigation.navigate('Menu');
            })
            .catch(err => {
                console.log(err);
                this.setState({loading: false});
                Alert.alert('Error', JSON.stringify(err));
            })

    };

    render() {
        return(
            <LinearGradient style={styles.container} colors={['#6e223b', '#d84d4d']}>
                <View style={styles.header}>
                    <Text style={styles.headerTitle}>¡Bienvenido!</Text>
                    <Text style={styles.headerSubtitle}>Introduce tu correo para iniciar sesión en la cuenta</Text>
                </View>
                <View>
                    <SignInForm onAuth={this.auth} />

                    <View style={{ alignItems: 'center'}}>
                        {this.state.loading? <ActivityIndicator size="large" color="#fff"/> : null}
                    </View>
                </View>
            </LinearGradient>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    header: {
        width: 300,
        marginTop: 30,
        marginBottom: 60,
    },
    headerTitle: {
        fontSize: 20,
        textAlign: 'center',
        color: 'white',
        marginBottom: 3
    },
    headerSubtitle: {
        fontSize: 18,
        textAlign: 'center',
        color: 'white'
    },
});

export default SignIn;